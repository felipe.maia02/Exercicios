#exercicio 7.1

list_convite = ['Pedrinha', 'Kyoto', 'Tio da limpeza']


print('Querido', list_convite[0] + ', quero que venha o jantar em minha casa.' )
print('Querido', list_convite[1] + ', quero que venha o jantar em minha casa.' )
print('Querido', list_convite[2] + ', quero que venha o jantar em minha casa.' )
print('----------------------------------------------------------------------------'
	  '                     Respostas dos convidados\n                             '
	  '----------------------------------------------------------------------------')
#exercicio 7.2

print(list_convite[0] + ', vai ao jantar')
print(list_convite[1] + ', vai ao jantar') 
nao_vai = 'Tio da limpeza'
list_convite.remove(nao_vai)
print(nao_vai.title() + ', não vai o jantar')
list_convite.append('Tia da limpeza')

print('Querido', list_convite[0] + ', como o', nao_vai, 'teve problemas estou mandando os convites com um novo convidado')
print('Querido', list_convite[1] + ', como o', nao_vai, 'teve problemas estou mandando os convites com um novo convidado')
print('Querida', list_convite[2] +',', 'o', nao_vai + ' não irá comparecer, então abriu uma vaga pra vc e será muito bem-vinda')

#exercicio 7.3

print('----------------------------------------------------------------------------'
	  '                             Novos convidados\n                             '
	  '----------------------------------------------------------------------------')

print(list_convite[0],'achei uma mesa maior, vou chamar mais 3 convidados')
print(list_convite[1],'achei uma mesa maior, vou chamar mais 3 convidados')
print(list_convite[2],'achei uma mesa maior, vou chamar mais 3 convidados')

list_convite.insert(0, 'Rodolfo')
list_convite.insert(2, 'Amarilda')
list_convite.insert(5, 'José')

print('----------------------------------------------------------------------------'
	  '                             Enviando os convites\n                         '
	  '----------------------------------------------------------------------------')


print(list_convite[0] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')
print(list_convite[1] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')
print(list_convite[2] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')
print(list_convite[3] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')
print(list_convite[4] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')
print(list_convite[5] + ', como convidei mais 3 pessoas, estou enviando novamente os convites a todos.')

#exercicio 7.4

print('----------------------------------------------------------------------------'
	  '                               Removendo\n                                  '
	  '----------------------------------------------------------------------------')

print('Infelizmente, irei chamar apenas duas pessoas para o jantar.')

convidado_removido01 = list_convite.pop(5)
print(convidado_removido01 + ', Infelizmente vc não vai poder ir, não tem cadeira suficiente')
convidado_removido02 = list_convite.pop(4)
print(convidado_removido02 + ', Infelizmente vc não vai poder ir, não tem cadeira suficiente')
convidado_removido03 = list_convite.pop(3)
print(convidado_removido03 + ', Infelizmente vc não vai poder ir, não tem cadeira suficiente')
convidado_removido04 = list_convite.pop(2)
print(convidado_removido04 + ', Infelizmente vc não vai poder ir, não tem cadeira suficiente')

print('-------------------------------------------------')
print(list_convite[0] + ', vc ainda está convidado ao jantar')
print(list_convite[1] + ', vc ainda está convidado ao jantar')
print('-------------------------------------------------')
del list_convite[0]
del list_convite[0]
print(list_convite)


