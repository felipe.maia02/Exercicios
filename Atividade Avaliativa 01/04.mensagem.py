alfabeto = "abcdefghijklmnopqrstuvxz"

chave = 4

mensagem = input('Coloque a mensagem criptografada: ').lower()
mensagemcriptografada = ''

for char in mensagem:
    if char in alfabeto:
        posicao = alfabeto.find(char)
        novaposicao = (posicao + chave) % 24
        mensagemcriptografada = mensagemcriptografada + alfabeto[novaposicao]

    else:
        mensagemcriptografada = mensagemcriptografada + char

print(mensagemcriptografada)