insetos = {
    ('Mortana'):['Chenpolin', 'Miherta', 'Otemti', 'Chumelho', 'Retrombis'],
    ('Moreira'):['Alteris-Funchis', 'Milheta', 'Chumelho', 'Viveder','Retrombis',],
    ('Mariva'):['Alteris-Funchis', 'Milheta', 'Chumelho','Viveder', 'Retrombis']}

nome_inseto = True
nome_familia = ''

while nome_inseto:
    nome_familia = str(input('Qual a familia que vc deseja informações? '))
    try:
        if nome_familia.upper() not in {'MORTANA', 'MOREIRA', 'MARIVA'}:
            print('Nome invalido')

        elif nome_familia.upper() == 'MORTANA':
            print('A familia', nome_familia,' deu a lista dos insetos conhecidos por eles\n',
                  insetos[nome_familia][0], ': Carnivoro\n', insetos[nome_familia][1], ': Carnivoro e Plantas\n',
                  insetos[nome_familia][2], ': Carnivoro\n', insetos[nome_familia][3], ': Sucção e Plantas\n',
                  insetos[nome_familia][4], ': Sucção\n'                                                       )

        elif nome_familia.upper() == 'MOREIRA':
            print('A familia', nome_familia,' deu a lista dos insetos conhecidos por eles\n',
                  insetos[nome_familia][0], ': Plantas\n', insetos[nome_familia][1], ': Carnivoro e Plantas\n',
                  insetos[nome_familia][2], ': Plantas e Sucção\n', insetos[nome_familia][3], ': Plantas\n', insetos[nome_familia][4], ': Sucção')

        elif nome_familia.upper() == 'MARIVA':
            print('A familia', nome_familia, ' deu a lista dos insetos conhecidos por eles\n',
                  insetos[nome_familia][0], ': Plantas\n', insetos[nome_familia][1], ': Carnivoro e Plantas\n',
                  insetos[nome_familia][2], ': Plantas e Sucção\n', insetos[nome_familia][3], ': Plantas\n', insetos[nome_familia][4], ': Sucção')

    except KeyError:
        print('Nome começa com letra maiuscula ')