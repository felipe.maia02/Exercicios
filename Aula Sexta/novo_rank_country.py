global lista_country_pib

dict_country_pib  = {}

def rank_pib():
	rank = dict(sorted(dict_country_pib.items(), key = lambda x: x[1], reverse = True))
	return rank

def adiciton():
	Adiciton = 0
	for key,value in dict_country_pib.items():
		Adiciton += dict_country_pib[key]
	return adiciton	

def ask_country_pib(country = '', pib = 0):
	country = input('Qual o país desejado? ')
	pib = float(input('Qual o PIB desse país?(bilhões) '))

	try: 
		if country.lower() not in dict_country_pib:
			dict_country_pib.update({country:pib})

		elif country.isdigit():
			print('Coloque um nome de um país!')
			return ask_country_pib()	
		else:
			print('Não se pode ter países iguais')
			return ask_country_pib()

	except ValueError:
		print('O PIB deve conter apenas numeros!')			
	
	return dict_country_pib

def place_pib():
	rank= rank_pib()
	place = 1

	for key,value in rank.items():
		print('O', key, 'tem o PIB de', value, 'bilhões e sua colocação ficou de', str(place), 'lugar')
		place +=1

def search_pib():
	ask_pib = input('Qual o país você deseja reelembrar o PIB? ')
	if(ask_pib in dict_country_pib.keys()):
		search = dict_country_pib.get(ask_pib)
		print('O PIB de', ask_pib,'é', str(search), 'bilhões')

	else:
		print('Opa algo deu errado!')
		return search_pib()	

def interface(loop = True):
	while loop:
		select = input('-------------------------------------------\n'
			           'Deseja iniciar o programa (s/n)\n'
			           'para iniciar o bonus (b)\n'
			           '-------------------------------------------\n'
			           'Resposta: ')

		if select == 'n':
			print(place_pib())
			loop = False

		elif select == 's':
			ask_country_pib()
			adiciton()
			rank_pib()

		elif select == 'b':
			search_pib()

		else:
			print('Coloque uma resposta válida!')

print(interface())			
