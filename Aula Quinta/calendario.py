from datetime import datetime, date

def ask_cal(calendar = ' Informe a data(DD MM YYYY): '):
	try:
		ask_date = str(input(calendar))
		day = datetime.strptime(ask_date, '%d %m %Y' ).date()

	except (ValueError, TypeError):
		print('Coloque uma data valida')
		return

	tomorow = date.fromordinal(day.toordinal() + 1)
	yesterday = date.fromordinal(day.toordinal() - 1)

	tomorow = '{}/{}/{}'.format(tomorow.day, tomorow.month, tomorow.year)
	yesterday = '{}/{}/{}'.format(yesterday.day, yesterday.month, yesterday.year)

	print(tomorow)
	print(yesterday)


print(ask_cal())