'''
Projeto de caixa eletrônico que realiza 
saque, extrato, transferência e deposito 
'''
from atm_interface import *
from atm_model import *

def withdraw(identity):
#Função que realiza o saque, puxando da função perform_withdraw
	value = ask_int("Valor do saque: ")
	
	if not perform_withdraw(identity, value):
		print("Saque não pode ser realizado!")
		return

	print("Saque realizado com sucesso.\n" + 
	"Verifique se realmente recebeu R$" + str(value))	

def deposit(identity):
#Função que realiza o deposito, puxando da função perform_deposito	
	deposit = ask_float("Valor do deposito: ")

	if not perform_deposit(identity, value):
		print('O deposito não pode ser realizado!')
		return

def extract(identity):
#Função que retira do dicionario o valor que o usuario tem na conta
	print("Seu saldo atual é: " + str(accounts[identity][2]))

def transfer(identity):
#Função que realiza a tranferencia, puxando da função perfom_tranfer		
	who_transfer = ask_string('Coloque a ID para realizar a transferência: ')
	transfer_value = ask_float('Qual é o valor da transferência: ')

	if not perfom_tranfer(identity,who_transfer,transfer_value):
		print('A transferência não pode ser realizado')
		return

def change(identity):
#função que permite trocar de conta	
	return start()

def close(identity):
#função que finaliza o programa	
	print("Bye Bye! " + accounts[identity][0])
	exit()	

functions = {"1" : withdraw, "2": deposit, "3": extract, "4": transfer,"5" : change, "0": close}

def start():
#função que inicia o programa	
	identity = ask_string("ID: ")
	valid    = check_id(identity)

	if not valid:
		print("ERRO. USUÁRIIO NÃO IDENTIFICADO")
		return

	option = "enter"	
	while option != "0":	
		option = showOptions()
		functions[option](identity)


start()