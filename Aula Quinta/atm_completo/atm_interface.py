def ask_string(message):
#função que faz uma mensagem apartir de uma string	
	v = input(message)
	return v

def ask_int(message, error_message = "not a int value"):
#função que permite apenas numeros inteiros, se não for um numero inteiro aparece uma mensagem de erro	
	try:
		v = int(input(message))
		return v	
	except ValueError:
		print(error_message)
		return ask_int(message, error_message)

def ask_float(message, error_message = "not a float value"):
#função que permite apenas numeros float, se não for um numero float aparece uma mensagem de erro		
	try:
		v = float(input(message))
		return v	
	except ValueError:
		print(error_message)
		return ask_float(message, error_message)			



def showOptions():
#função que inicia a interface grafica a interage com as outra funções.	
	option = input("---------------------------\n" + 
		  "1 - Sacar\n" +  
		  "2 - Depositar\n" + 
		  "3 - Extrato\n" + 
		  "4 - Transferência\n" +
		  "5 - Trocar de conta\n" + 
		  "0 - Sair\n\n" +
		  "Opção: ")

	return option