'''
Projeto de caixa eletrônico que realiza 
saque, extrato, transferência e deposito 
'''

accounts = {"AX476T": ["José Renato Martins", "007.008.009-00", 23.56],
			"BF896T": ["Bolsonaro", "007.007.007-77", 2847233.56],
			"BATMAN": ["Batman", "000.000.000-01", 289999947233.56],
			"AXXX67": ["Iron Man", "123.007.007-77", -522324341.56]}

def do_withdraw():
	try:
		do_withdraw = float(input('Valor do saque: '))

	
	except ValueError:
		print('Coloque um valor valido!')
		return
	return do_withdraw	

def do_deposit():
	try:
		do_deposit = float(input('Valor do deposito: '))

	except ValueError:
		print('Coloque um valor valido!')
		return 
	return do_deposit			


def withdraw(identity):
	accounts[identity][2] = accounts[identity][2] - do_withdraw()
	print('Você tem agora na conta', accounts[identity][2])
 
def deposit(identity):
	accounts[identity][2] = accounts[identity][2] + do_deposit()
	print('Você tem agora na conta', accounts[identity][2])

def extract(identity):
	print('Saldo atual:', accounts[identity][2])

def transfer(identity):
	print("transfer!")

def close(identity):
	print("Bye Bye!")
	exit()		


			
functions = {"1" : withdraw, "2": deposit, "3": extract, "4": transfer, "0": close}


def askID():
	ID = input("Type your ID: ")
	return ID

def showOptions():
	option = input("---------------------------\n" + 
		  "1 - Sacar\n" +  
		  "2 - Depositar\n" + 
		  "3 - Extrato\n" + 
		  "4 - Transferência\n" + 
		  "0 - Sair\n\n" +
		  "Opção: ")

	return option


def checkID(identity):

	try:
		accounts[identity]
	except (KeyError, ValueError):	
		return False

	return True	

def start():
	identity = askID()
	valid    = checkID(identity)

	if not valid:
		print("ERRO. USUÁRIIO NÃO IDENTIFICADO")
		return

	option = True	
	while option != "0":	
		option = showOptions()
		functions[option](identity)



start()