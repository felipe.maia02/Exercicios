'''
Projeto de caixa eletrônico que realiza 
saque, extrato, transferência e deposito 
'''

accounts = {"AX476T": ["José Renato Martins", "007.008.009-00", 23.56],
			"BF896T": ["Bolsonaro", "007.007.007-77", 2847233.56],
			"BATMAN": ["Batman", "000.000.000-01", 289999947233.56],
			"AXXX67": ["Iron Man", "123.007.007-77", -522324341.56]}

def do_withdraw():
'''
Função que pergunta ao usuario o valor do saque
'''
	try:
		do_withdraw = float(input('Valor do saque: '))

	
	except ValueError:
		print('Coloque um valor valido!')
		return
	return do_withdraw	

def do_deposit():
'''
Função que pergunta ao usuario o valor do deposito
'''	
	try:
		do_deposit = float(input('Valor do deposito: '))

	except ValueError:
		print('Coloque um valor valido!')
		return 
	return do_deposit			


def withdraw(identity):
'''
Pega a função do valor do saque e retira da lista o valor desejado
'''
	accounts[identity][2] = accounts[identity][2] - do_withdraw()
	print('Você tem agora na conta', accounts[identity][2])
 
def deposit(identity):
'''
Pega a função do valor do deposito e adiciona o valor da lista o valor desejado
'''
	
	accounts[identity][2] = accounts[identity][2] + do_deposit()
	print('Você tem agora na conta', accounts[identity][2])

def extract(identity):
'''
Procura dentro da lista o valor do extrato
'''	
	print('Saldo atual:', accounts[identity][2])

def transfer(identity):
	print("transfer!")

def close(identity):
	print("Bye Bye!")
	exit()		


			
functions = {"1" : withdraw, "2": deposit, "3": extract, "4": transfer, "0": close}


def askID():
'''
Função que pergunta qual é a ID
'''	
	ID = input("Type your ID: ")
	return ID

def showOptions():
'''
Função com um menu 'grafico' aonde se o valor da ID for valido inicia
'''	
	option = input("---------------------------\n" + 
		  "1 - Sacar\n" +  
		  "2 - Depositar\n" + 
		  "3 - Extrato\n" + 
		  "4 - Transferência\n" + 
		  "0 - Sair\n\n" +
		  "Opção: ")

	return option


def checkID(identity):
'''
Se o valor da ID não for valido fecha o progama, se tiver okay continua
'''
	try:
		accounts[identity]
	except KeyError:	
		return False

	return True	

def start():
'''
Aqui é aonde passa o controle das funções e verifica se as condições do usuario estão corretas
'''
	identity = askID()
	valid    = checkID(identity)

	if not valid:
		print("ERRO. USUÁRIIO NÃO IDENTIFICADO")
		return

	option = True	
	while option != "0":	
		option = showOptions()
		functions[option](identity)



start()