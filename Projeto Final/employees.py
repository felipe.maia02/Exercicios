class Employees:
	def __init__(self, name, registration, cpf):
		self.name         = name
		self.registration = registration
		self.cpf		  = cpf

	#OBS: Apenas um usuario ADM pode usar essas funções para armazenar os funcionarios!
	#função de armazenar o funcionario
	def to_Eemployee(self):
		return(self.name         + '|'
			   self.registration + '|'
			   self.cpf          + '|')

	def	employee_read(self):
		print('Nome do contribuinte: ' + self.name         + 
			  'Matricula: '            + self.registration +
			  'CPF: '                  + self.cpf          + '\n')
	