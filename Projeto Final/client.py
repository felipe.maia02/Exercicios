class Client:
	def __init__(self, name, cpf, cell_number, adress, cep):
		
		self.name        = name
		self.cpf         = cpf
		self.cell_number = cell_number
		self.adress      = adress
		self.cep         = cep

	#colocar uma função para mover o cliente para o banco de dados
	def to_Client(self):
		return (self.name        + '|' +
				self.cpf         + '|' +
				self.cell_number + '|' +
				self.adress      + '|' +
				self.cep         + '|' + '\n')


	#colocar uma função para pegar informações de clientes já cadastrados
	def client_read(self):
		#transformar o cliente em string
		print('Nome: '    + self.name        + 
			  'CPF: '     + self.cpf         + 
			  'Número: '  + self.cell_number +
			  'Endereço: '+ self.adress      +
			  'CEP: '     + self.cep         + '\n')