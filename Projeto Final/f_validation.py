#Classe de validação / str / int / float
class Validation():

	def ask_str(self, mensage):
		mens = input(mensage)
		return mens

	def ask_int(self, mensage, error_mensage = 'Coloque número valido!'):
		try:
			num = int(input(mensage))
			return num

		except ValueError:
			print(error_mensage)
			return(mensage, error_mensage)

	def ask_float(self, mensage, error_mensage = 'Coloque número válido!'):
		try:
			num_f = float(input(mensage))
			return num_f

		except ValueError:
			print(error_mensage)
			return(mensage, error_mensage)
