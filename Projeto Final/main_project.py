from f_validation import Validation
from databank import Databank
from employees import Employees
from client import Client
import os

class Mainproject:
	def register_client(self):

		vali = Validation()

		name        = vali.ask_str('Nome: '               )
		cpf         = vali.ask_str('CPF: '                )
		cell_number = vali.ask_str('Número para contado: ')
		address     = vali.ask_str('Endereço: '           )
		cep         = vali.ask_str('CEP: '                )

		clients = Client(name, cpf, cell_number, address, cep)
		
		data    = Databank()
		data.writeClient(clients.to_Client(), 'client.cl')

	def register_employee(self):
		vali = Validation()
		
		name         = vali.ask_str('Nome: '               )
		registration = vali.ask_str('Matricula: '          )
		cpf          = vali.ask_str('CPF: '                )

		employees = Employees(name,registration, cpf)

		data02 = Databank()
		data02.writeClient(employees.to_Employee(), 'employees.cl')
