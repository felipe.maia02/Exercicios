import random

'''
Definição do tabuleiro da roleta.
O tabuleiro é representado por um dicionário CHAVE-VALOR.

CHAVE: valor númerico inteiro da casa do tabuleiro.
VALOR: lista com as características associadas a casa do tabuleiro.
      
    NUMERO:  [<COR>,<TERCO>,<METADE>,<PARIDADE>], onde:
	
	NUMERO   | string | valores no intervalo de 0 a 36
	COR      | string | "black", "red" ou "green"
	TERCO    | string | "first", "second", "third" ou "none"
	METADE   | string | "first-half", "second-half" ou "none"
	PARIDADE | string | "odd", "even" ou "none"

'''            
roulette_board = {0: ["green" , "none"  , "none"       , "none"],
			      1: ["black" , "first" , "first-half" , "odd" ],
				  2: ["red"   , "first" , "first-half" , "even"],
				  3: ["black" , "first" , "first-half" , "odd" ],
				  4: ["red"   , "first" , "first-half" , "even"],
				  5: ["black" , "first" , "first-half" , "odd" ],
				  6: ["red"   , "first" , "first-half" , "even"],
				  7: ["black" , "first" , "first-half" , "odd" ],
				  8: ["red"   , "first" , "first-half" , "even"],
				  9: ["black" , "first" , "first-half" , "odd" ],
				  10:["red"   , "first" , "first-half" , "even"],
				  11:["black" , "first" , "first-half" , "odd" ],
				  12:["red"   , "first" , "first-half" , "even"],
				  13:["black" , "second", "first-half" , "odd" ],
				  14:["red"   , "second", "first-half" , "even"],
				  15:["black" , "second", "first-half" , "odd" ],
				  16:["red"   , "second", "first-half" , "even"],
				  17:["black" , "second", "first-half" , "odd" ],
				  18:["red"   , "second", "first-half" , "even"],
				  19:["black" , "second", "second-half", "odd" ],
				  20:["red"   , "second", "second-half", "even"],
				  21:["black" , "second", "second-half", "odd" ],
				  22:["red"   , "second", "second-half", "even"],
				  23:["black" , "second", "second-half", "odd" ],
				  24:["red"   , "second", "second-half", "even"],
				  25:["black" , "third" , "second-half", "odd" ],
				  26:["red"   , "third" , "second-half", "even"],
				  27:["black" , "third" , "second-half", "odd" ],
				  28:["red"   , "third" , "second-half", "even"],
				  29:["black" , "third" , "second-half", "odd" ],
				  30:["red"   , "third" , "second-half", "even"],
				  31:["black" , "third" , "second-half", "odd" ],
				  32:["red"   , "third" , "second-half", "even"],
				  33:["black" , "third" , "second-half", "odd" ],
				  34:["red"   , "third" , "second-half", "even"],
				  35:["black" , "third" , "second-half", "odd" ],
				  36:["red"   , "third" , "second-half", "even"]}

'''
Funções para verificação das cores de um determinado valor
'''

def check_color(value, color):
	try:
		return roulette_board[value][0] == color
	except KeyError:
		# TODO: deve ser criado um erro específico para esse caso
		return 'ERRO DE TABULEIRO'

def is_red(value):
	return check_color(value, "red")
	
def is_black(value):
	return check_color(value, "black")

def is_green(value):
	return check_color(value, "green")	

						
'''
Funções básicas
'''
# função que fornecido um valor e um terço indica se o valor é pertencente a esse terço
def check_palce(value, place):
	try:
		return roulette_board[value][1] == place
	except KeyError:
		return 'ERRO DE TABULEIRO'	
# função que indica se uma determinada casa está no primeiro terço
def check_first(value):
	return check_palce(value, 'first')

# função que fornecido um valor e uma metade indica se o valor é pertencente a essa metade
def check_half(value, half):
	try:
		return roulette_board[value][2] == half
	except KeyError:
		return'ERRO DE TABULEIRO'

# função que indica se uma determinada casa está no segundo terço
def check_second(value):
	return check_palce(value, 'second')
# função que indica se uma determinada casa está no terceiro terço
def check_third(value):
	return check_palce(value, 'third')
	
	
# função que indica se uma determinada casa está na primeira metade
def check_frist_half(value):
	return check_half(value, 'first-half')
	

# função que indica se uma determinada casa está no segunda metade
def check_second_half(value):
	return check_half(value, 'second-half')

#validar impar/par
def check_odd_even(value, number):
	try:
		return roulette_board[value][3] == number
	except KeyError:
		return'ERRO DE TABULEIRO'

# função que indica se o número é par
def check_odd(value):
	return check_odd_even(value, 'odd')

# função que indica se o número é impar
def check_even(value):
	return check_odd_even(value, 'even')

'''
Funções práticas
'''

# função que fornecida uma aposta e a casa sorteada indica se a aposta foi vencedora
def check_result(bet, result):
	try:
		if bet == result:
			return True	

		elif bet in roulette_board[result]:
			return True

		else:
			return False	
	except ValueError:
		return'ERRO DE TABULEIRO'
				
# função que gera um valor aleatório entre 0 e 36
def random_number():
	try:
		return random.randint(0,36)
	except ValueError:
		return'ERRO DE TABULEIRO'



'''
Funções avançadas
'''

# função que fornecida a aposta e o valor da aposta, retorne o lucro ou prejuizo
	# uma aposta em um número dobra o valor apostado
	# uma aposta em um terco aumenta em 20% o valor apostado
	# uma aposta em uma metade, cor, ou paridade, aumenta em 5% o valor apostado 

# função que armazene as apostas realizadas e o nome do apostador

# função que verifica em uma lista de apostas o resultado de cada uma delas

'''
Funções especiais
'''

# função que fornecido o nome de uma pessoa retorne quanto ela tem de lucro ou de prejuizo 

# crie uma validação que não permita que uma pessoa aposte caso ela esteja devendo mais que 1000 reais 

# função que execute um menu da seguinte forma:
	# 1. apostar
	# 2. fechar ciclo de apostas
	# 3. fechar a banca

    # a opção 1 adiciona uma nova aposta na lista de apostas.
    # a opção 2 realiza o sorteio e indica os vencedores com seus respectivos lucros ou prejuizos
    # a opção 3 fecha o programa, informando o saldo da banca. 
'''
Pré-requisitos
'''

# Ao iniciar o jogo a banca sempre começa com o saldo de R$ 100.000,00

# Caso algum apostador ganhe deve ser retirado o dinheiro da banca.
# Caso contrário deve ser adicionado o valor da aposta na banca.

# O jogo deve funcionar completamente. 
# Todos os erros possíveis devem ser tratados