from random import randint
print('-=-'*8)
print('Vamos jogar Jokenpô?')
print('-=-'*8)
# pedra = 1 / papel = 2 / tesoura = 3
jg = str(input('Escolha pedra, papel ou tesoura: ')).strip().lower()
pc = randint(1, 3)
if pc == 1 and jg == 'pedra' or pc == 2 and jg == 'papel' or pc == 3 and jg == 'tesoura':
    print('Empatamos')
    if pc == 1:
        print('Eu escolhi pedra')
    elif pc == 2:
        print('Eu escolhi papel')
    else:
        print('Eu escolhi tesoura')
elif pc == 1 and jg == 'tesoura' or pc == 2 and jg == 'pedra' or pc == 3 and jg == 'papel':
    print('Eu venci')
    if pc == 1:
        print('Eu escolhi pedra')
    elif pc == 2:
        print('Eu escolhi papel')
    else:
        print('Eu escolhi tesoura')
elif pc == 1 and jg == 'papel' or pc == 2 and jg == 'tesoura' or pc == 3 and jg == 'pedra':
    print('Você venceu')
    if pc == 1:
        print('Eu escolhi pedra')
    elif pc == 2:
        print('Eu escolhi papel')
    else:
        print('Eu escolhi tesoura')