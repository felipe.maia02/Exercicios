from tkinter import *

class Application:
	def __init__ (self, master=None):
		self.primeiroContainer = Frame(master)
		self.primeiroContainer["pady"] = 10
		self.primeiroContainer.pack()

		self.segundoContainer = Frame(master)
		self.segundoContainer["padx"] = 20
		self.segundoContainer.pack()

		self.terceiroContainer = Frame(master)
		self.terceiroContainer["padx"] = 20
		self.terceiroContainer.pack()

		self.quartoContainer = Frame(master)
		self.quartoContainer["pady"] = 0
		self.quartoContainer.pack()

		self.quintoContainer = Frame(master)
		self.quintoContainer["pady"] = 0
		self.quintoContainer.pack()

		self.sextoContainer = Frame(master)
		self.sextoContainer['pady'] = 0
		self.sextoContainer.pack()

		self.setimoContainer = Frame(master)
		self.setimoContainer['pady'] = 0
		self.setimoContainer.pack()

		self.oitavoContainer = Frame(master)
		self.oitavoContainer['pady'] = 0
		self.oitavoContainer.pack()


		self.titulo = Label(self.primeiroContainer, text="Calcular Soma")
		self.titulo["font"] = ("Arial", "12", "bold")
		self.titulo.pack()

		self.n1Label = Label(self.segundoContainer, text="Primeiro Número:", font=("Courier New", "12"))
		self.n1Label.pack(side=LEFT)

		self.n1 = Entry(self.segundoContainer)
		self.n1["width"] = 10
		self.n1["font"] = ("Arial", "12")
		self.n1.pack(side=LEFT)

		self.n2Label = Label(self.terceiroContainer, text=" Segundo Número:", font=("Courier New", "12"))
		self.n2Label.pack(side=LEFT)

		self.n2 = Entry(self.terceiroContainer)
		self.n2["width"] = 10
		self.n2["font"] = ("Arial", "12")
		self.n2.pack(side=LEFT)

		self.somar = Button(self.quartoContainer)
		self.somar["text"] = "Somar"
		self.somar["font"] = ("Arial", "12", "bold")
		self.somar["width"] = 12
		self.somar["command"] = self.soma
		self.somar.pack()

		self.subtrair = Button(self.quintoContainer)
		self.subtrair["text"] = "Subtrair"
		self.subtrair["font"] = ('Arial', '12', 'bold')
		self.subtrair['width'] = 12
		self.subtrair['command'] = self.subtração
		self.subtrair.pack()

		self.multiplicar = Button(self.sextoContainer)
		self.multiplicar["text"] = "Multiplicar"
		self.multiplicar["font"] = ('Arial', '12', 'bold')
		self.multiplicar['width'] = 12
		self.multiplicar['command'] = self.multiplicação
		self.multiplicar.pack()

		self.dividir = Button(self.sextoContainer)
		self.dividir["text"] = "divisão"
		self.dividir["font"] = ('Arial', '12', 'bold')
		self.dividir['width'] = 12
		self.dividir['command'] = self.divisão
		self.dividir.pack()

		self.mensagem = Label(self.oitavoContainer, text="", font=("Arial", "14"))
		self.mensagem["fg"]="blue"
		self.mensagem.pack()


# Método para somar
	def soma(self):
		num1 = float(self.n1.get())
		num2 = float(self.n2.get())
		s=num1+num2
		self.mensagem["text"] = "A soma é " + str(s)

	def subtração(self):
		num1 = float(self.n1.get())
		num2 = float(self.n2.get())
		sub=num1 - num2
		self.mensagem['text'] = 'A subtração é ' + str(sub)

	def multiplicação(self):
		num1 = float(self.n1.get())
		num2 = float(self.n2.get())
		m = num1 * num2
		self.mensagem['text'] = 'A Multiplicação é ' + str(m)

	def divisão(self):
		num1 = float(self.n1.get())
		num2 = float(self.n2.get())
		d = num1 / num2
		self.mensagem['text'] = 'A Divisão é ' + str(d)

root = Tk()
root.title("Janela para somar ...")