invalid = True

while invalid:
	try:
		idade = int(input('Quantos anos você tem? '))
		semestre = int(input('Qual semestre você está: '))
		atrasado = int(input('Quantos semestres você reprovou? '))
		formula01 = float(idade + (semestre/2))
		formula02 = float(formula01 + atrasado/2)
		formula02_str = float(formula02/12)

		print(round(formula02_str), ' anos até se formar')

	except ValueError:
		print('Coloque um valor valido!')		